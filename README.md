<h1>Projet Semestre 2 Java</h1>

<h2>Membres du Groupe : </h2>
<ul>
	<li>Mathieu Picart</li>
	<li>Mickaël Urien</li>
	<li>Mathys Henry</li>
	<li>Thibault Pichon</li>
	<li>Guillaume Dalles</li>
</ul>

<h2>Tuteur du Projet : </h2>
<ul>
    <li>Arnaud Martin</li>
</ul>

<h2>Elements du projets : </h2>
<ul>
	<li>Sources : Codes sources du projet Eclipse </li>
	<li>Maquettes : Maquettes du logiciel prévues avant le début du développement</li>
</ul>
