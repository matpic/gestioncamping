package Controller.model;

public class Emplacement {
	
	/**
	 * Tableau de chaines de characteres des diff�rentes disponibilit�s (Disponible, Occup� ou Reserv�).
	 * 
	 */
	public final static String[] disponibilite = {"Disponible","Occup�","R�serv�"};
	
	/**
	 * Le nombre d'emplacement actuel. Ce num�ro s'incr�mente � la cr�ation d'un emplacement.
	 * 
	 */
	private static int nbEmplacement = 1;
	
	/**
     * Le num�ro de l'emplacement. Ce num�ro d'emplacement est changeable.
     * 
     * @see Emplacement#getNumEmplacement()
     * @see Emplacement#setNumEmplacement(int)
     * 
     */
	private int numEmplacement;
	
	/**
	 * La disponibilit� de l'emplacement, elle peut �tre �gale � "Disponible", "Occup�" ou "Reserv�". Cette chaine de caracteres est modifiable.
	 * 
	 * @see Emplacement#getDispo()
	 * @see Emplacement#setDispo(String)
	 * 
	 */
	private String dispo;
	
	/**
	 * Le type de l'emplacement, il s'agit d'une chaine qui determine si c'est un emplacement pour une tente, un mobil-home ou une carave. Cette chaine de caracteres est modifiable.
	 * 
	 * @see Emplacement#getType()
	 * @see Emplacement#setType(String)
	 * 
	 */
	private String type;
	
	/**
	 * Boolean permettant de savoir si un client est attrivu� � cette emplacement. Ce boolean est modifiable.
	 * 
	 * @see Emplacement#getClientAttribue()
	 * @see Emplacement#setClientAttribue(boolean)
	 * 
	 */
	private boolean clientAttribue;
	
	
	/**
	 * Constructeur de la classe Emplacement.
	 * Initialise l'emplacement avec une disponibilit�, un type et un boolean {@link Emplacement#clientAttribue} en param�tres.
	 * 
	 * @param dispo La disponibilit� de l'emplacement
	 * @param type Le type de l'emplacement
	 * @param clientAttribue Le boolean permettant de savoir si un client est attribu� ou non � cette emplacement
	 * 
	 */
	public Emplacement(String dispo, String type, boolean clientAttribue) {
		this.numEmplacement = nbEmplacement++;
		this.dispo = dispo;
		this.type = type;
		this.clientAttribue = clientAttribue;
	}
	
	
	/**
	 * Constructeur de la classe Emplacement.
	 * Initialise l'emplacement avec un num�ro d'emplacement, une disponibilit� et un type en param�tres.
	 * 
	 * @param numEmplacement Le num�ro de l'emplacement.
	 * @param dispo La disponibilit� de l'emplacement.
	 * @param type Le type de l'emplacement.
	 * 
	 */
	public Emplacement(int numEmplacement, String dispo, String type) {
		this.numEmplacement = numEmplacement;
		this.dispo = dispo;
		this.type = type;
		this.clientAttribue = false;
	}

	/**
	 * Constructeur de la classe Emplacement.
	 * Initialise l'emplacement avec un num�ro d'emplacement initialis� � la cr�ation par incr�mentation, une disponibilit� et un type vide ainsi qu'un boolean a "false".
	 *
	 */
	public Emplacement() {
		this.numEmplacement = nbEmplacement++;
		this.dispo = "";
		this.type = "";
		this.clientAttribue = false;
	}
	
	/**
	 * Methode retournant en chaine de caracteres le num�ro, le nom et le prenom de l'emplacement.
	 *
	 * @return le num�ro, la disponibilit� et le type de l'emplacement en une chaine de caracteres.
	 * 
	 * @see Emplacement#afficher()
	 * 
	 */
	@Override
	public String toString() {
		return "Emplacement n�" + numEmplacement + " | Disponibilit� : " + dispo + " | Type : " + type
				+ " | Client attribu� : " + this.getClientAttribue();
	}
	
	/**
	 * Methode permettant l'affichage d'un client 
	 * 
	 */
	public void afficher() {
		System.out.println(this.toString());
	}
	
	
	/** ACCESSEURS **/

	/**
     * Retourne le num�ro de l'emplacement.
     * 
     * @return numEmplacement Le num�ro de l'emplacement correspondant, sous forme d'un entier.
     * 
     */
	public int getNumEmplacement() {
		return numEmplacement;
	}

	/**
     * Met � jour le num�ro de l'emplacement.
     * 
     * @param numEmplacement Le num�ro de l'emplacement.
     * 
     */
	public void setNumEmplacement(int numEmplacement) {
		this.numEmplacement = numEmplacement;
	}

	/**
     * Retourne la disponibilit� de l'emplacement.
     * 
     * @return dispo La disponibilit� de l'emplacement correspondant, sous forme d'une chaine de caract�res.
     * 
     */
	public String getDispo() {
		return dispo;
	}
	
	/**
     * Met � jour la disponibilit� de l'emplacement.
     * 
     * @param dispo La disponibilit� de l'emplacement.
     * 
     */
	public void setDispo(String dispo) {
		this.dispo = dispo;
	}

	/**
     * Retourne le type de l'emplacement.
     * 
     * @return type Le type de l'emplacement correspondant, sous forme d'une chaine de caract�res.
     * 
     */
	public String getType() {
		return type;
	}

	/**
     * Met � jour le type de l'emplacement.
     * 
     * @param type Le type de l'emplacement.
     * 
     */
	public void setType(String type) {
		this.type = type;
	}

	/**
     * Retourne le boolean permettant de savoir si un client est attribu� � l'emplacement.
     * 
     * @return clientAttribue Le boolean de l'emplacement correspondant, sous forme d'un boolean.
     * 
     */
	public boolean getClientAttribue() {
		return clientAttribue;
	}

	/**
     * Met � jour le boolean permettant de savoir si un client est attribu� � l'emplacement.
     * 
     * @param clientAttribue Le boolean de l'emplacement.
     * 
     */
	public void setClientAttribue(boolean clientAttribue) {
		this.clientAttribue = clientAttribue;
	}


}
