package Controller.model;

public class Reservation {
	
	/**
	 * Le nombre de r�servation actuel. Ce num�ro s'incr�mente � la cr�ation d'une r�servation.
	 * 
	 */
	private static int nbR = 1;
	
	/**
     * Le num�ro de r�servation. Ce num�ro de r�servation est changeable.
     * 
     * @see Reservation#getNumReservation()
     * @see Reservation#setNumReservation(int)
     * 
     */
	private int numReservation;
	
	/**
	 * Le num�ro de l'emplacement li� � la r�servation. Ce num�ro d'emplacement est changeable.
	 * 
	 * @see Reservation#getNum_empl()
	 * @see Reservation#setNum_empl(int)
	 * 
	 */
	private int num_empl;
	
	/**
	 * Le type de l'emplacement li� � la r�servation. Ce type d'emplacement est changeable.
	 * 
	 * @see Reservation#getNum_empl()
	 * @see Reservation#setNum_empl(int)
	 * 
	 */
	private String type_empl;
	
	
	/**
	 * Le client li� � la r�servation. Ce client est changeable.
	 * 
	 * @see Reservation#getClient()
	 * @see Reservation#setClient(Client)
	 * 
	 */
	private Client client;
	
	/**
	 * L'employ� li� � la r�servation. Cet employ� est changeable.
	 * 
	 * @see Reservation#getEmploye()
	 * @see Reservation#setEmploye(Employe)
	 * 
	 */
	private Employe employe;
	
	/**
	 * La date d'arriv� du client. Cette date est changeable.
	 * 
	 * @see Reservation#getDateArrivee()
	 * @see Reservation#setDateArrivee(String)
	 * 
	 */
	private String dateArrivee;
	
	/**
	 * La date de d�part du client. Cette date est changeable.
	 * 
	 * @see Reservation#getDateDepart()
	 * @see Reservation#setDateDepart(String)
	 * 
	 */
	private String dateDepart;
	
	
	/**
	 * Constructeur de la classe Reservation.
	 * Initialise la r�servation avec un num�ro d'emplacement, un type d'emplacement, un client, un employe, une date d'arriv�e et une date de d�part en param�tres.
	 * 
	 * @param num_empl Le num�ro de l'emplacement li� � la r�servation.
	 * @param type_empl Le type de l'emplacement li� � la r�servation.
	 * @param client Le client li� � la r�servation.
	 * @param employe L'employ� li� � la r�servation.
	 * @param dateArrivee La date d'arriv�e du client.
	 * @param dateDepart La date de d�part du client.
	 * 
	 */
	public Reservation(int num_empl,String type_empl, Client client, Employe employe, String dateArrivee, String dateDepart) {
		this.numReservation = nbR++;
		this.num_empl = num_empl;
		this.type_empl = type_empl;
		this.client = client;
		this.employe = employe;
		this.dateArrivee = dateArrivee;
		this.dateDepart = dateDepart;
	}
	
	
	/**
	 * Constructeur de la classe Reservation.
	 * Initialise la r�servation avec un num�ro de r�servation, un num�ro d'emplacement, un type d'emplacement, un client, un employe, une date d'arriv�e et une date de d�part en param�tres.
	 * 
	 * @param numResservation Le num�ro de r�servation.
	 * @param num_empl Le num�ro de l'emplacement li� � la r�servation.
	 * @param type_empl Le type de l'emplacement li� � la r�servation.
	 * @param client Le client li� � la r�servation.
	 * @param employe L'employ� li� � la r�servation.
	 * @param dateArrivee La date d'arriv�e du client.
	 * @param dateDepart La date de d�part du client.
	 * 
	 */
	public Reservation(int numResservation, int num_empl, String type_empl, Client client, Employe employe, String dateArrivee, String dateDepart) {
		this.numReservation = numResservation;
		this.num_empl = num_empl;
		this.type_empl = type_empl;
		this.client = client;
		this.employe = employe;
		this.dateArrivee = dateArrivee;
		this.dateDepart = dateDepart;
	}

	/**
	 * Methode retournant en chaine de caracteres les attributs de la classe Reservation.
	 *
	 * @return le nom et le prenom du client en une chaine de caracteres.
	 * 
	 * @see Reservation#afficherReservation()
	 * 
	 */
	@Override
	public String toString() {
		return "Reservation n�" + this.getNumReservation() + " | \nEmplacement n� : " + this.getNum_empl() + " Type : " + this.getType_empl() + " |"
				+ " \nClient n� : " + this.getClient() + " | \nEmploye n� : " + this.getEmploye() 
				+  " | \nDate d'arrivee : " + this.getDateArrivee() + " | Date de depart : " + this.getDateDepart();
	}
	
	/**
	 * Methode permettant l'affichage d'une r�servation. 
	 * 
	 */
	public void afficherReservation() {
		System.out.println(this.toString());
	}
	
	/** ACCESSEURS **/
	
	/**
     * Retourne le num�ro de la r�servation.
     * 
     * @return numReservation Le num�ro de la r�servation correspondant, sous forme d'un entier.
     * 
     */
	public int getNumReservation() {
		return numReservation;
	}

	/**
     * Met � jour le num�ro de la r�servation.
     * 
     * @param numReservation Le num�ro de la r�servation.
     * 
     */
	public void setNumReservation(int numReservation) {
		this.numReservation = numReservation;
	}

	/**
     * Retourne le num�ro de l'emplacement li� � la r�servation.
     * 
     * @return num_empl Le num�ro de l'emplacement li� � la r�servation correspondant, sous forme d'un entier.
     * 
     */
	public int getNum_empl() {
		return num_empl;
	}

	/**
     * Met � jour le num�ro de l'emplacement li� � la r�servation.
     * 
     * @param num_empl Le num�ro de l'emplacement li� � la r�servation.
     * 
     */
	public void setNum_empl(int num_empl) {
		this.num_empl = num_empl;
	}

	/**
     * Retourne le type de l'emplacement li� � la r�servation.
     * 
     * @return type_empl Le type de l'emplacement li� � la r�servation correspondant, sous forme d'une chaine de caracteres.
     * 
     */
	public String getType_empl() {
		return type_empl;
	}

	/**
     * Met � jour le type de l'emplacement li� � la r�servation.
     * 
     * @param type_empl Le type de l'emplacement li� � la r�servation.
     * 
     */
	public void setType_empl(String type_empl) {
		this.type_empl = type_empl;
	}

	/**
     * Retourne le client li� � la r�servation.
     * 
     * @return client Le client li� � la r�servation correspondant, sous forme d'une instance de Client.
     * 
     */
	public Client getClient() {
		return client;
	}

	/**
     * Met � jour le client li� � la r�servation.
     * 
     * @param client Le client li� � la r�servation.
     * 
     */
	public void setClient(Client client) {
		this.client = client;
	}

	/**
     * Retourne l'employ� li� � la r�servation.
     * 
     * @return employe L'employ� li� � la r�servation correspondant, sous forme d'une instance de Employe.
     * 
     */
	public Employe getEmploye() {
		return employe;
	}

	/**
     * Met � jour l'employ� li� � la r�servation.
     * 
     * @param employe L'employ� li� � la r�servation.
     * 
     */
	public void setEmploye(Employe employe) {
		this.employe = employe;
	}

	/**
     * Retourne la date d'arriv�e du client li� � la r�servation.
     * 
     * @return dateArrivee La date d'arriv�e du client li� � la r�servation correspondant, sous forme d'une chaine de caracteres.
     * 
     */
	public String getDateArrivee() {
		return dateArrivee;
	}

	/**
     * Met � jour la date d'arriv�e du client li� � la r�servation.
     * 
     * @param dateArrivee La date d'arriv�e du client li� � la r�servation.
     * 
     */
	public void setDateArrivee(String dateArrivee) {
		this.dateArrivee = dateArrivee;
	}

	/**
     * Retourne la date de d�part du client li� � la r�servation.
     * 
     * @return dateDepart La date de d�part du client li� � la r�servation correspondant, sous forme d'une chaine de caracteres.
     * 
     */
	public String getDateDepart() {
		return dateDepart;
	}

	/**
     * Met � jour la date de d�part du client li� � la r�servation.
     * 
     * @param dateDepart La date de d�part du client li� � la r�servation.
     * 
     */
	public void setDateDepart(String dateDepart) {
		this.dateDepart = dateDepart;
	}

}
