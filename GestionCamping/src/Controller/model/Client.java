package Controller.model;


public class Client extends Personne {
	
	/**
	 * Le nombre de client actuel. Ce num�ro s'incr�mente � la cr�ation d'un client.
	 * 
	 */
	private static int nbClients = 1;
	
	/**
     * Le num�ro de client. Ce num�ro de client est changeable.
     * 
     * @see Client#getNumClient()
     * @see Client#setNumClient(int)
     */
	private int numClient;
	
	/**
     * Le code postal du client. Ce code postal du client est changeable.
     * 
     * @see Client#getCodePostal()
     * @see Client#setCodePostal(int)
     */
	private int codePostal;
	
	/**
     * Le telephone du client. Ce num�ro de telephone du client est changeable.
     * 
     * @see Client#getTelephone()
     * @see Client#setTelephone(String)
     */
	private String telephone;

	/**
	 * Constructeur de la classe Client.
	 * Initialise le client avec un nom, un prenom, un code postal et un telephone en param�tres.
	 *
	 * @param nom Le nom du client.
	 * @param prenom Le prenom du client.
	 * @param codePostal Le code postal du client.
	 * @param telephone Le telephone du client.
	 */
	public Client(String nom, String prenom, int codePostal, String telephone) {
		super(nom, prenom);
		this.numClient = nbClients++;
		this.codePostal = codePostal;
		this.telephone = telephone;
	}
	
	/**
	 * Constructeur de la classe Client.
	 * Initialise le client avec un num�ro de client, un nom, un prenom, un code postal et un telephone en param�tres.
	 * 
	 * @param nom Le nom du client.
	 * @param prenom Le prenom du client.
	 * @param numClient Le num�ro du client.
	 * @param codePostal Le code postal du client.
	 * @param telephone Le telephone du client.
	 */
	public Client(int numClient, String nom, String prenom, int codePostal, String telephone) {
		super(nom, prenom);
		this.numClient = numClient;
		this.codePostal = codePostal;
		this.telephone = telephone;
	}

	/**
	 * Constructeur de la classe Client.
	 * Initialise le client avec un num�ro de client initialis� � la cr�ation par incr�mentation, un nom et un prenom vide, un code postal � 0 et un telephone avec une chaine vide.
	 *
	 */
	public Client() {
		super();
		this.numClient = nbClients++;
		this.codePostal = 0;
		this.telephone = "";
	}
	
	/**
	 * Methode retournant en chaine de caracteres le nom et le prenom du client. Elle fait appel � la methode {@link Personne#toString()}.
	 *
	 * @return le nom et le prenom du client en une chaine de caracteres.
	 * 
	 * @see Personne#toString()
	 * @see Client#afficherClient()
	 * 
	 */
	public String toString() {
		return super.toString();
	}
	
	/**
	 * Methode permettant l'affichage d'un client 
	 * 
	 */
	public void afficherClient() {
		System.out.println(this.toString());
	}

	/** ACCESSEURS **/
	
	/**
     * Retourne le num�ro du client.
     * 
     * @return numClient Le num�ro du client correspondant, sous forme d'un entier.
     * 
     */
	public int getNumClient() {
		return numClient;
	}

	/**
     * Met � jour le num�ro du client.
     * 
     * @param numClient Le num�ro du client.
     * 
     */
	public void setNumClient(int numClient) {
		this.numClient = numClient;
	}

	/**
     * Retourne le code postal du client.
     * 
     * @return codePostal Le code postal du client correspondant, sous forme d'un entier.
     * 
     */
	public int getCodePostal() {
		return codePostal;
	}

	/**
     * Met � jour le code postal du client.
     * 
     * @param codePostal Le code postal du client.
     * 
     */
	public void setCodePostal(int codePostal) {
		this.codePostal = codePostal;
	}

	/**
     * Retourne le telephone du client.
     * 
     * @return telephone Le telephone du client correspondant, sous forme d'une chaine de caract�res.
     * 
     */
	public String getTelephone() {
		return telephone;
	}

	/**
     * Met � jour le telephone du client.
     * 
     * @param telephone Le telephone du client.
     * 
     */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}
	
}
