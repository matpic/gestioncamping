package Controller.model;

/* **************************************************************************************** */
/* **********************************  Data Class  **************************************** */
/* **************************************************************************************** */
/* **************************************************************************************** */
/* ****  Class that make the link between DB and View  ************************************ */
/* **************************************************************************************** */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class Data {
	private static String lien = "jdbc:sqlite:Camping.db";
	private static Connection conn ;
	private static Statement st ;
	private static PreparedStatement ps ;
	private static ResultSet rs ;	
	
	/**
     * Constructeur de la classe Data
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public Data() {
		try {
			conn = DriverManager.getConnection(lien);
			st = conn.createStatement();
		} catch(SQLException e) {
			System.out.println("Echec (Exception SQL) : " + e.getMessage());
		}
	}
	
	/* **************************************************************************************** */
	/* *****************************  GETTERS AND SETTERS  ************************************ */
	/* **************************************************************************************** */
	
	/**
     * Methode qui permet de retourner une instance de Connection
     * 
     * @return Connection
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static Connection getConn() {
		return conn;
	}

	/**
     * Methode qui permet de retourner une instance de Statement
     * 
     * @return Statement
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
    public static Statement getSt()
    {
       return Data.st; 
    }
    
    /**
     * Methode qui permet de retourner une instance de ResultSet
     * 
     * @return ResultSet
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
    public static ResultSet getRs() {
        return rs;
    }

    /**
     * Methode qui permet de fermer les connexions � la base de donn�es
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void fermerConnexion() {
		try { conn.close(); } catch (SQLException e) { System.out.println("ERREUR Fermeture Connection" + e.getMessage()); }
//		try { st.close(); } catch (SQLException e) { System.out.println("ERREUR Fermeture Connection" + e.getMessage()); }
//		try { rs.close(); } catch (SQLException e) { System.out.println("ERREUR Fermeture Connection" + e.getMessage()); }
	}
	
	
	/**********************************************************************************************************************************/
	/***************************  CLIENT  ********************************************************************************************/
	/********************************************************************************************************************************/
	
	/**
     * Permet de r�cuperer un client � partir de son num�ro de client
     * 
     * @return le Client correspondant � l'id pass�e en param�tre
     * 
     * @param id : le num�ro du Client � r�cuperer
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static Client recupererClientDepuisId(int id) {
		String query = "SELECT * FROM Client WHERE numClient = " + id;
		Client cli = null;
		try
        {
            PreparedStatement ps;
            ResultSet rs;
            conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            cli = new Client(rs.getInt("numClient"),rs.getString("nomClient"),rs.getString("prenomClient"),rs.getInt("codePostal"),rs.getString("telephone"));
            conn.close();
        }catch (SQLException e)
        {
        	e.printStackTrace();
        }
		
		return cli;
	}
	
	/**
     * Permet de r�cuperer le nom du client � partir de son num�ro de client
     * 
     * @return le nom du Client correspondant � l'id pass�e en param�tre
     * 
     * @param id : le num�ro du Client � r�cuperer
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static String recupererNomClientDepuisId(int id) {
		String query = "SELECT nomClient FROM Client WHERE numClient = " + id;
		String cli = null;
		try
        {
            conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            cli = rs.getString("nomClient");
            
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
		return cli;
	}
	
	/**
     * Permet recuperer l'ensemble des noms des clients dans une collection de Client
     *
     * @return cli  retroune une ArrayList de String, l'ensemble des noms des clients
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ArrayList<String> recupererNomClient() {
		ArrayList<String> cli = new ArrayList<String>();
		String query = "SELECT nomClient, prenomClient FROM Client";
		try
        {
			conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
            	cli.add(rs.getString("nomClient") + " " + rs.getString("prenomClient"));
            }
            
            
        }catch (SQLException e)
        {
            e.printStackTrace();
        } finally {
        	fermerConnexion();
        }
		return cli;
	}
	
	/**
     * Permet d'ajouter un client � la table Client de la base de donn�es
     *
     * @param c le client � ajouter dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void ajouterClient(Client c) {
        String query = "INSERT INTO Client (nomClient,prenomClient,codePostal,telephone) VALUES ('" + c.getNom() + "','" + c.getPrenom() + "','" + c.getCodePostal() + "','" + c.getTelephone() + "')";
        try
        {
            conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.execute(query);
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        

    }
	
	/**
     * Permet de supprimer un client dans la table Client de la base de donn�es
     *
     * @param id : le num�ro du client � supprimer dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void supprimerClient(int id) {
		String query = "DELETE FROM Client WHERE numClient = " + id;
		try
        {
			conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
	}
	
	/**
     * Permet de modifier un client dans la table Client de la base de donn�es
     *
     * @param c : le client � modifier dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void modifierClient(Client c)
    {
        if(existe("Client","numClient",c.getNumClient()))
        {
            try
            {
            	String query = 	"UPDATE Client set nomClient = '" + c.getNom() + "',  prenomClient = '" + c.getPrenom() + 
        				"', codePostal = " + c.getCodePostal() + ", telephone = '" + c.getTelephone() + 
        				"' WHERE numClient = " + c.getNumClient();
            	conn = DriverManager.getConnection(lien);
                st = conn.createStatement();
                st.executeUpdate(query);
                conn.close();
            }catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
        else{
            System.out.println("Impossible de traiter votre demande.");
        }
     
    }
	
	/**
     * Permet recuperer l'ensemble des clients dans une collection de Client
     *
     * @return listeClients  retourne une ArrayList de Client, l'ensemble des clients
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ArrayList<Client> recupererClients()
    {
        ArrayList<Client> listeClients = new ArrayList<Client>();

        String query = "SELECT * FROM Client";

        try
        {
        	conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                listeClients.add(new Client(rs.getInt("numClient"),rs.getString("nomClient"),rs.getString("prenomClient"), rs.getInt("codePostal"), rs.getString("telephone")));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listeClients;
    }
	
	/**
     * Permet recuperer l'ensemble des clients dans une collection de Client
     *
     * @return listeClients retourne une ObservableList de Client, l'ensemble des clients
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ObservableList<Client> obsListClients()
    {
        ObservableList<Client> listeClients = FXCollections.observableArrayList();
        try
        {
        	String query = "SELECT * FROM Client";
        	conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
                listeClients.add(new Client(rs.getInt("numClient"),rs.getString("nomClient"),rs.getString("prenomClient"), rs.getInt("codePostal"), rs.getString("telephone")));

            }
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        return listeClients;
    }
	
	/**
     * Permet d'afficher � l'�cran l'ensemble des clients
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void afficherClient()
    {
        ArrayList<Client> c = recupererClients();
        for(Client cli : c)
        {
            cli.afficherClient();
        }
    }
	
	/**********************************************************************************************************************************/
	/**********************  EMPLACEMENT  ********************************************************************************************/
	/********************************************************************************************************************************/
	
	/**
     * Permet de r�cuperer un emplacement � partir de son num�ro d'emplacement
     * 
     * @return l'emplacement correspondant � l'id pass�e en param�tre
     * 
     * @param id : le num�ro de l'emplacement � r�cuperer
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	private static Emplacement recupererEmplacementDepuisId(int id) {
		String query = "SELECT * FROM Emplacement WHERE numEmplacement = " + id;
		Emplacement empl = null;
		try
        {
            PreparedStatement ps;
            ResultSet rs;
            conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
        
            empl = new Emplacement(rs.getInt("numEmplacement"),rs.getString("dispo"),rs.getString("type"));
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
		
		return empl;
	}
	
	/**
     * Permet d'ajouter un emplacement � la table Emplacement de la base de donn�es
     *
     * @param emplacement : l'emplacement � ajouter dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void ajouterEmplacement(Emplacement emplacement) {
        String query = "INSERT INTO Emplacement (dispo,type) VALUES ('" + emplacement.getDispo() + "','" + emplacement.getType() + "')";
        try
        {
        	conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.execute(query);
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        
	}
	
	/**
     * Permet de supprimer un emplacement dans la table Emplacement de la base de donn�es
     *
     * @param id : le num�ro de l'emplacement � supprimer dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void supprimerEmplacement(int id) {
		String query = "DELETE FROM Emplacement WHERE ID = " + id;
		try
        {
			conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
      
	}
	
	/**
     * Permet de modifier un emplacement dans la table Emplacement de la base de donn�es
     *
     * @param emp : l'emplacement � modifier dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void modifierEmplacement(Emplacement emp)
    {
        if(existe("Emplacement","numEmplacement",emp.getNumEmplacement()))
        {
            String query = "UPDATE Emplacement SET dispo='" + emp.getDispo() + "', type='" + emp.getType() + "' WHERE numEmplacement = " + emp.getNumEmplacement() +";";
            try
            {
            	conn = DriverManager.getConnection(lien);
            	st = conn.createStatement();
                st.executeUpdate(query);
                conn.close();
            }catch (SQLException e)
            {
            	System.out.println("Echec (Exception SQL) : " + e.getMessage());
            }
        }
        else{
            System.out.println("Impossible de traiter votre demande.");
        }
        
        
    }
	
	/**
     * Permet recuperer l'ensemble des emplacements dans une collection d'Emplacement
     *
     * @return listeEmplacements retourne une ArrayList d'Emplacement : l'ensemble des emplacements
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ArrayList<Emplacement> recupererEmplacements()
    {
        ArrayList<Emplacement> listeEmplacements = new ArrayList<Emplacement>();
        String query = "SELECT * FROM Emplacement";

        try
        {
        	conn = DriverManager.getConnection(lien);
        	st = conn.createStatement();
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next())
            {
            	listeEmplacements.add(new Emplacement(rs.getInt("numEmplacement"),rs.getString("dispo"),rs.getString("type")));
            }
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
       
        return listeEmplacements;
    }
	
	/**
     * Permet d'afficher � l'�cran l'ensemble des emplacements
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void afficherEmplacements()
    {
        ArrayList<Emplacement> e = recupererEmplacements();
        System.out.println("Liste des emplacements :");
        for(Emplacement emp : e)
        {
            emp.afficher();
        }
    }
	
	/**********************************************************************************************************************************/
	/**************************  EMPLOYE  ********************************************************************************************/
	/********************************************************************************************************************************/
	
	/**
     * Permet de r�cuperer un employe � partir de son num�ro d'employe
     * 
     * @return employe correspondant � l'id pass�e en param�tre
     * 
     * @param id : le num�ro de l'employe � r�cuperer
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	private static Employe recupererEmployeDepuisId(int id) {
		String query = "SELECT * FROM Employe WHERE numEmploye = " + id;
		Employe employe = null;
		try
        {
			Connection conn;
            PreparedStatement ps;
            ResultSet rs;
            conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            employe = new Employe(rs.getInt("numEmploye"),rs.getString("nom"),rs.getString("prenom"));
            conn.close();
    		
            
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
		return employe;
	}
	
	/**
     * Permet recuperer l'ensemble des noms des employes dans une collection d'Employe
     *
     * @return emp retourne une ArrayList de String, l'ensemble des noms des employes
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ArrayList<String> recupererNomEmploye() {
		ArrayList<String> emp = new ArrayList<String>();
		String query = "SELECT nom, prenom FROM Employe";
		try
        {
			conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next()) {
            	emp.add(rs.getString("nom") + " " + rs.getString("prenom"));
            }
            conn.close();
            
        }catch (SQLException e)
        {
            e.printStackTrace();
        } 
		return emp;
	}

	/**
     * Permet d'ajouter un employe � la table Employe de la base de donn�es
     *
     * @param employe : l'employe � ajouter dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void ajouterEmploye(Employe employe) {
        String query = "INSERT INTO Employe (nom,prenom) VALUES ('" + employe.getNom() + "','" + employe.getPrenom() + "')";
        try
        {
        	conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.execute(query);
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
       
	}
	
	/**
     * Permet de supprimer un employe dans la table Employe de la base de donn�es
     *
     * @param id : le num�ro de l'employe � supprimer de la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void supprimerEmploye(int id) {
		String query = "DELETE FROM Employe WHERE numEmploye = " + id;
		try
        {
			conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
       
	}
	
	/**
     * Permet de modifier un employe dans la table Employe de la base de donn�es
     *
     * @param e : l'employe � modifier dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void modifierEmploye(Employe e)
    {
        if(existe("Employe","numEmploye",e.getNumEmploye()))
        {
            String query = "UPDATE Employe set nom = '" + e.getNom() + "',  prenom = '" + e.getPrenom() + "' WHERE numEmploye = '" + e.getNumEmploye() + "';" ;
            try
            {
            	conn = DriverManager.getConnection(lien);
                st = conn.createStatement();
                st.executeUpdate(query);
                conn.close();
            }catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }
        else
        {
            System.out.println("Impossible de traiter votre demande.");
        }
       
    }
	
	/**
     * Permet recuperer l'ensemble des employes dans une collection d'Employe
     *
     * @return listeEmployes retourne une ArrayList d'Employe : l'ensemble des employes
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ArrayList<Employe> recupererEmployes()
    {
        ArrayList<Employe> listeEmployes = new ArrayList<Employe>();
        String query = "SELECT * FROM Employe";
        try
        {
        	conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
           
            while(rs.next())
            {
            	listeEmployes.add(new Employe(rs.getInt("numEmploye"),rs.getString("nom"),rs.getString("prenom")));
            }
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        } 
        return listeEmployes;
    }
	
	/**
     * Permet recuperer l'ensemble des employes dans une collection d'Employe
     *
     * @return listeEmployes retourne une ObservableList d'Employe, l'ensemble des employes
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ObservableList<Employe> obsListEmployes()
    {
        ObservableList<Employe> listeEmployes = FXCollections.observableArrayList();
        try
        {
        	String query = "SELECT * FROM Employe";
        	conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            while(rs.next())
            {
            	listeEmployes.add(new Employe(rs.getInt("numEmploye"),rs.getString("nom"),rs.getString("prenom")));
            }
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        } 
        return listeEmployes;
    }
	
	/**
     * Permet d'afficher � l'�cran l'ensemble des employes
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void afficherEmployes()
    {
        ArrayList<Employe> e = recupererEmployes();
        for(Employe emp : e)
        {
            emp.afficher();
        }
    }
	
	/**********************************************************************************************************************************/
	/**********************  RESERVATION  ********************************************************************************************/
	/********************************************************************************************************************************/
	
	/**
     * Permet d'ajouter une reservation � la table Reservation de la base de donn�es
     *
     * @param r : la reservation � ajouter dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void ajouterReservation(Reservation r) {
        String query = "INSERT INTO Reservation (numEmplacement,numClient,numEmploye,dateA,dateD) "
        		+ "VALUES ('" + r.getNum_empl() + "','" + r.getClient().getNumClient() + "','" 
        		+ r.getEmploye().getNumEmploye() + "','" + r.getDateArrivee() + "','" + r.getDateDepart() + "')";
        try
        {
        	conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.execute(query);
            st.close();
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        }
        
	}
	
	/**
     * Permet de supprimer une reservation dans la table Reservation de la base de donn�es
     *
     * @param id : le num�ro de la reservation � supprimer de la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void supprimerReservation(int id) {
		try
        {
			String query = "DELETE FROM Reservation WHERE numRes = " + id ;
			conn = DriverManager.getConnection(lien);
            st = conn.createStatement();
            st.executeUpdate(query);
            conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        } 
	}
	
	public static void supprimerListReservation(ArrayList<Reservation> r) {
		try
        {
			
			for(Reservation res : r) {
				conn = DriverManager.getConnection(lien);
				String query = "DELETE FROM Reservation WHERE numRes = '" + res.getNumReservation() + "';"
						+ "UPDATE Emplacement SET dispo = 'Disponible' WHERE numEmplacement = " + res.getNum_empl() + ";";
	            st = conn.createStatement();
	            st.executeUpdate(query);
	            conn.close();
	            
	        }
			conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        } 
	}
	
	public static void supprimerReservationParEmp(int idemp) {
		try
        {
			conn = DriverManager.getConnection(lien);
			String query = "DELETE FROM Reservation WHERE numEmplacement = '" + idemp + "';"
						+ "UPDATE Emplacement SET dispo = 'Disponible' WHERE numEmplacement = " + idemp + ";";
	        st = conn.createStatement();
	        st.executeUpdate(query);
	        conn.close();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        } 
	}
	
	/**
     * Permet de modifier une reservation dans la table Reservation de la base de donn�es
     *
     * @param res : la reservation � modifier dans la base de donn�es
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void modifierReservation(Reservation res)
    {
        if(existe("Reservation","numRes",res.getNumReservation()))
        {
            String query = "UPDATE Reservation SET dateA = '" + res.getDateArrivee() + "',  dateD = '" + res.getDateDepart() + "' WHERE numRes = '" + res.getNumReservation() + "';" ;
            try
            {
            	conn = DriverManager.getConnection(lien);
                st = conn.createStatement();
                st.executeUpdate(query);
            	conn.close();
            }catch (SQLException ex)
            {
                ex.printStackTrace();
            }
        }
        else{
            System.out.println("Impossible de traiter votre demande.");
        }
       
    }
	
	/**
     * Permet recuperer l'ensemble des reservations dans une collection de Reservation
     *
     * @return listeReservations : retourne une ArrayList de Reservation, l'ensemble des reservations
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ArrayList<Reservation> recupererReservations()
    {
        ArrayList<Reservation> listeReservations = new ArrayList<Reservation>();

        String query = "SELECT * FROM Reservation";

        try
        {
        	PreparedStatement ps;
            ResultSet rs;
        	conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next())
            {
            	Emplacement emplacement = recupererEmplacementDepuisId(rs.getInt("numEmplacement"));
            	Employe employe = recupererEmployeDepuisId(rs.getInt("numEmploye"));
            	Client client = recupererClientDepuisId(rs.getInt("numClient"));
            	listeReservations.add(new Reservation(rs.getInt("numRes"),emplacement.getNumEmplacement(),emplacement.getType(),client,employe,rs.getString("dateA"),rs.getString("dateD")));
            }
            conn.close();
        }catch (SQLException e)
        {
            e.printStackTrace();
        } 
        return listeReservations;
    }
	
	/**
     * Permet de r�cuperer une reservation � partir d'un emplacement
     * 
     * @return reservation : la reservation correspondant au num�ro d'emplacement pass� en parametre
     * 
     * @param id : le num�ro de l'emplacement 
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static Reservation recupererReservationDepuisEmp(int id)
	{
		String query = "SELECT * FROM Reservation WHERE numEmplacement = " + id;
		Reservation reservation = null;
		try
        {
            PreparedStatement ps;
            ResultSet rs;
            conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            if(rs.next()) {
            	if(((rs.getInt("numEmplacement")>=1)&&(rs.getInt("numClient")>=1))) {
                 	Emplacement emplacement = recupererEmplacementDepuisId(rs.getInt("numEmplacement"));
                 	Employe employe = recupererEmployeDepuisId(rs.getInt("numEmploye"));
                 	Client client = recupererClientDepuisId(rs.getInt("numClient"));
                     reservation = new Reservation(rs.getInt("numRes"),emplacement.getNumEmplacement(),emplacement.getType(),client,employe,rs.getString("dateA"),rs.getString("dateD"));
                 } else {
                 	System.out.println("Pas de r�servation pour l'emplacement " + id);
                 }  
            }
            conn.close();
        } 
		catch (SQLException e)
        {
        	System.out.println("Echec (Exception SQL) : " + e.getMessage());
        } 
		
		return reservation;
    }
	
	/**
     * Permet recuperer l'ensemble des reservations dans une collection de Reservation
     *
     * @return listeReservations retourne une ObservableList de Reservation : l'ensemble des reservations
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static ObservableList<Reservation> obsListReservations()
    {
        ObservableList<Reservation> listeReservations = FXCollections.observableArrayList();
        try {
        	String query = "SELECT * FROM Reservation";
        	conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while(rs.next()) {
            	Emplacement emplacement = recupererEmplacementDepuisId(rs.getInt("numEmplacement"));
            	Employe employe = recupererEmployeDepuisId(rs.getInt("numEmploye"));
            	Client client = recupererClientDepuisId(rs.getInt("numClient"));
            	listeReservations.add(new Reservation(rs.getInt("numRes"),
            			emplacement.getNumEmplacement(),emplacement.getType(),client,employe,rs.getString("dateA"),rs.getString("dateD")));

            }
            conn.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
        return listeReservations;
    }
	
	/**
     * Permet d'afficher � l'�cran l'ensemble des reservations
     *
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static void afficherReservations()
    {
        ArrayList<Reservation> e = recupererReservations();
        for(Reservation r : e) {
            r.afficherReservation();
        }
    }

	/* ************************************************************************************************************************************** */
	/**
     * Requete modulable permetant de savoir si enregistrement existe d�j�
     * 
     * @param  table , la table ou on doit faire la requ�te
     * @param  attribut , sur quel attribut la requ�te doit �tre effectuer
     * @param  valeur , la valeur que doit prendre l'attribut
     * 
     * @return boolean Un bool�en renvoyant vrai si l'enregistrement existe, faux dans les autres cas
     * 
     * @throws SQLEXCEPTION  Si jamais il y a un quelconque probl�me avec la base de donn�es
     */
	public static boolean existe(String table,String attribut,int valeur)
    {
		
        // Intialisation du bool�en
        boolean test = false;
        try
        {
        	String query = "SELECT * FROM " + table +" WHERE "+ attribut + " = " + valeur;
        	// Selection en fonction des param�tres
            conn = DriverManager.getConnection(lien);
            ps  = conn.prepareStatement(query);
            rs = ps.executeQuery();
            // J'execute ma requete
            if (!rs.isBeforeFirst() ) {
                // Si le curseur est avant le premier r�sulat => Pas de r�sultat
                test = false;
            }
            else{
                test = true;
                // Sinon le r�sultat existe
            }
            conn.close();
        }catch (SQLException e)
        {
        	e.printStackTrace();
        }
       
        return test;
    }
}
