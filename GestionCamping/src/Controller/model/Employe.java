package Controller.model;

public class Employe extends Personne {
	
	/**
	 * Le nombre d'employ� actuel. Ce num�ro s'incr�mente � la cr�ation d'un employ�.
	 * 
	 */
	private static int nbEmployes = 1;
	
	/**
     * Le num�ro de l'employ�. Ce num�ro de l'employ� n'est pas changeable.
     * 
     * @see Employe#getNumEmploye()
     * @see Employe#setNumEmploye(int)
     */
	private int numEmploye;

	/**
	 * Constructeur de la classe Employe.
	 * Initialise l'employ� avec un nom et un prenom en param�tres. Le num�ro de l'employ� est initialis� � la cr�ation par incr�mentation.
	 * 
	 * @param nom Le nom de l'employ�.
	 * @param prenom Le prenom de l'employ�.
	 * 
	 */
	public Employe(String nom, String prenom) {
		super(nom, prenom);
		this.numEmploye = nbEmployes++;
	}

	/**
	 * Constructeur de la classe Employe.
	 * Initialise l'employ� avec un num�ro d'employ�, un nom et un prenom en param�tres.
	 * 
	 * @param numEmploye Le num�ro de l'employ�.
	 * @param nom Le nom de l'employ�. 
	 * @param prenom Le nom de l'employ�.
	 * 
	 */
	public Employe(int numEmploye, String nom, String prenom) {
		super(nom, prenom);
		this.numEmploye = numEmploye;
	}

	/**
	 * Constructeur de la classe Employe.
	 * Initialise l'employ� avec un num�ro d'employ� initialis� � la cr�ation par incr�mentation, le nom et le prenom sont vides.
	 * 
	 */
	public Employe() {
		super();
		this.numEmploye = nbEmployes++;
	}

	/**
	 * Methode retournant en chaine de caracteres le nom et le prenom de l'employ�. Elle fait appel � la methode {@link Personne#toString()}.
	 *
	 * @return le nom et le prenom de l'employ� en une chaine de caracteres.
	 * 
	 * @see Personne#toString()
	 * @see Employe#afficher()
	 */
	public String toString() {
		return super.toString();
	}
	
	/**
	 * Methode permettant l'affichage d'une personne 
	 * 
	 */
	public void afficher() {
		System.out.println(this.toString());
	}
	
	/** ACCESSEURS **/
	
	/**
     * Retourne le num�ro de l'employ�.
     * 
     * @return Le num�ro de l'employ� correspondant, sous forme d'un entier.
     * 
     */
	public int getNumEmploye() {
		return numEmploye;
	}

	/**
     * Met � jour le num�ro de l'employ�.
     * 
     * @param numEmploye Le num�ro de l'employ�.
     * 
     */
	public void setNumEmploye(int numEmploye) {
		this.numEmploye = numEmploye;
	}

}
