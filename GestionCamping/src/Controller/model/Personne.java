package Controller.model;

public abstract class Personne {
	
	/**
     * Le nom de la personne. Ce nom est changeable.
     * 
     * @see Personne#getNom()
     * @see Personne#setPrenom(String)
     */
	private String nom;
	
	/**
     * Le prenom de la personne. Ce prenom est changeable.
     * 
     * @see Personne#getPrenom()
     * @see Personne#setPrenom(String)
     */
	private String prenom;

	/**
	 * Constructeur de la classe Personne.
	 * Initialise la personne avec un nom et un prenom entr� en param�tres.
	 *
	 * @param nom Le nom de la personne
	 * @param prenom Le prenom de la personne
	 *
	 */
	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}
	
	/**
	 * Constructeur de la classe Personne.
	 * Initialise la personne avec un nom et un prenom vide.
	 * 
	 */
	public Personne() {
		this.nom = " ";
		this.prenom = " ";
	}
	
	/**
	 * Methode retournant en chaine de caracteres le nom et le prenom de la personne.
	 *
	 * @return le nom et le prenom de la personne en une chaine de caracteres.
	 */
	public String toString() {
		return this.getNom() + " " + this.getPrenom();
	}

	
	/**
	 * Methode permettant l'affichage d'une personne 
	 * 
	 */
	public void afficherPersonne() {
		System.out.println(this.toString());
	}
	
	/** ACCESSEURS **/
	
	/**
     * Retourne le nom de la personne.
     * 
     * @return Le nom correspondant, sous forme d'une chaine de caract�res.
     * 
     */
	public String getNom() {
		return nom;
	}
	
	/**
     * Met � jour le nom de la personne.
     * 
     * @param nom Le nouveau nom de la personne.
     * 
     */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
     * Retourne le prenom de la personne.
     * 
     * @return Le prenom correspondant, sous forme d'une chaine de caract�res.
     * 
     */
	public String getPrenom() {
		return prenom;
	}

	/**
     * Met � jour le prenom de la personne.
     * 
     * @param prenom Le nouveau prenom de la personne.
     * 
     */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
}
