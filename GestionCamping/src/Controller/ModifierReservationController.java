package Controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import Controller.model.Data;
import Controller.model.Reservation;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ModifierReservationController extends Stage implements Initializable {

	private Stage stage;
	private Reservation reserv;
	
	@FXML
	private Button bn_Annuler;
	@FXML
	private Button bn_Enregistrer;
	@FXML
	private TextField tNum;
	@FXML
	private TextField tType;
	@FXML
	private TextField tClient;
	@FXML
	private TextField tEmp;
	@FXML
	private DatePicker dateArr;
	@FXML
	private DatePicker dateDep;
	
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.tNum.setText(Integer.toString(reserv.getNumReservation()));
		this.tType.setText(reserv.getType_empl());
		
		if (reserv.getClient() != null) {
			this.tClient.setText(reserv.getClient().getNom() + " " + reserv.getClient().getPrenom());
		}
		if (reserv.getEmploye() != null) {
			this.tEmp.setText(reserv.getEmploye().getNom() + " " + reserv.getEmploye().getPrenom());
		}
		if (reserv.getNum_empl() > 0) {
			this.tNum.setText(Integer.toString(reserv.getNum_empl()));
		}
		if (reserv.getType_empl() != null) {
			this.tType.setText(reserv.getType_empl());
		}
		this.dateArr.setValue(LocalDate.now());
		this.dateArr.setPromptText(reserv.getDateArrivee());
		this.dateDep.setValue(LocalDate.now());
		this.dateDep.setPromptText(reserv.getDateDepart());
		
		Callback<DatePicker, DateCell> dayCellFactory= this.getDayCellFactory();
        this.dateArr.setDayCellFactory(dayCellFactory);
        this.dateDep.setDayCellFactory(dayCellFactory);
		
		this.tClient.setEditable(false);
		this.tEmp.setEditable(false);
		this.tNum.setEditable(false);
		this.tType.setEditable(false);
		
	}
	
	public ModifierReservationController(Reservation reserv) {
		stage = new Stage();
		this.reserv = reserv;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ModifierReserv.fxml"));
			
			loader.setController(this);
			this.stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("Modifier une réservation");
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		
		this.reserv.getDateArrivee();	    
	}

	@FXML
	public void confModif(ActionEvent e) {
		reserv.setDateArrivee(this.dateArr.getPromptText());
		System.out.println(reserv.getDateArrivee().toString());
		reserv.setDateDepart(this.dateDep.getPromptText());
		System.out.println(reserv.getDateDepart().toString());
		Data.modifierReservation(reserv);
		stage.close();
	}
	
	private Callback<DatePicker, DateCell> getDayCellFactory() {
		 
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
 
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
 
                        // Disable Monday, Tueday, Wednesday.
                        if (item.getYear() < LocalDate.now().getYear()) {
                            this.setDisable(true);
                            this.setStyle("-fx-background-color: #ffc0cb;");
                        } else if (item.getYear() == LocalDate.now().getYear()) {
                        	if(item.getDayOfYear() < LocalDate.now().getDayOfYear()) {
                        		this.setDisable(true);
                                this.setStyle("-fx-background-color: #ffc0cb;");
                        	}
                        }
                    }
                };
            }
        };
        return dayCellFactory;
    }
	
	@FXML
	void annuler(ActionEvent e) {
		stage.close();
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
}
