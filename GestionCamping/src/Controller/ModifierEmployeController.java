package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.gluonhq.charm.glisten.control.TextField;

import Controller.model.Data;
import Controller.model.Employe;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModifierEmployeController extends Stage implements Initializable{

	@FXML
	private TextField tNom;
	@FXML
	private TextField tPrenom;
	@FXML
	private Button bn_modif;
	@FXML
	private Label label_conf;
	
	private Stage stage;
	private Employe emp;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.tNom.setText(emp.getNom());
		this.tPrenom.setText(emp.getPrenom());
		this.label_conf.setVisible(false);
		
	}
	
	public ModifierEmployeController(Employe emp) {
		stage = new Stage();
		this.emp = emp;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ModifierEmploye.fxml"));
			
			loader.setController(this);
			this.stage.initModality(Modality.APPLICATION_MODAL);
			Scene scene = new Scene(loader.load());
			
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Modifier un employ�");
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		this.tNom.setOnKeyReleased(e -> {
			
			if(		this.tPrenom.getText().isEmpty() ||
					this.tNom.getText().isEmpty()) {
				
				this.bn_modif.setDisable(true);
			
			}else {
				this.bn_modif.setDisable(false);
			}
				
			if (contientChiffre(this.tNom.getText())){
				this.bn_modif.setDisable(true);
				this.label_conf.setVisible(true);
				this.tNom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tNom.styleProperty().set(null);
					
			}
				
			
		});
		
		this.tPrenom.setOnKeyReleased(e -> {
			
			if(		this.tPrenom.getText().isEmpty() ||
					this.tNom.getText().isEmpty()) {
				
				this.bn_modif.setDisable(true);
			
			}else {
				this.bn_modif.setDisable(false);
			}
				
			if (contientChiffre(this.tPrenom.getText())){
				this.bn_modif.setDisable(true);
				this.label_conf.setVisible(true);
				this.tPrenom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tPrenom.styleProperty().set(null);
					
			}
				
			
		});
	}
	
	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){ 
			stage.close();
	     }
		
		if (key.getCode() == KeyCode.ENTER){
			this.bn_modif.fire();
	     }
		
	}
	
	public boolean contientChiffre(String str) {
		return (!str.matches("[a-zA-Z-�-]*"));
	}
	
	@FXML
	public void confModif(ActionEvent e) {
		emp.setNom(this.tNom.getText());
		emp.setPrenom(this.tPrenom.getText());
		Data.modifierEmploye(emp);
		stage.close();
	}
	
	@FXML
	void annuler(ActionEvent e) {
		stage.close();
	}
	
	
	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}

}
