package Controller;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MenuController extends Stage implements Initializable {

	@FXML private GridPane gridpane;
	@FXML private SplitPane splitpane;
	
    @FXML
    private Button btn_client;
    
    @FXML
    private Button btn_reserv;
    
    @FXML
    private Label date;
    
    public static Date dateDuJour = new Date();
    private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    private Stage stage;
    
    
    @Override
	public void initialize(URL location, ResourceBundle resources) {
    	
    	this.date.setText(dateFormat.format(dateDuJour));	
	}
    
    
    public MenuController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/Menu.fxml"));
			
			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("Camping");
			stage.setMaximized(true);
					
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
    
    
    
    @FXML
    void listeClientScene(ActionEvent event) throws IOException {    	
    	ListeClientController listeCli = new ListeClientController();
    	listeCli.getStage().show();
    	this.stage.close();
    }
 

    @FXML
    void listeEmployeScene(ActionEvent event) throws IOException {    	
    	ListeEmployeController listeEmp = new ListeEmployeController();
    	listeEmp.getStage().show();
    	this.stage.close();

    }
    
    @FXML
    void listeReservationScene(ActionEvent event) throws IOException {
    	ListeReservController listeReserv = new ListeReservController();
    	listeReserv.getStage().show();
    	this.stage.close();

    }
    
    @FXML
    void gererEmplacementScene(ActionEvent event) throws IOException {
    	GererEmplacementController gererEmplacement = new GererEmplacementController();
    	gererEmplacement.getStage().show();
    	this.stage.close();

    }



	public Stage getStage() {
		return stage;
	}



	public void setStage(Stage stage) {
		this.stage = stage;
	}
    
    

}
