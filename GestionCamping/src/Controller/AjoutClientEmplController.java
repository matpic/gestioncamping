
package Controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ResourceBundle;

import Controller.model.Client;
import Controller.model.Data;
import Controller.model.Emplacement;
import Controller.model.Employe;
import Controller.model.Reservation;
import Controller.view.ButtonEmp;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AjoutClientEmplController extends Stage implements Initializable{

	@FXML
	private DatePicker dateArr;
	@FXML
	private DatePicker dateDep;
	@FXML
	private ComboBox<Client> combo_client;
	@FXML
	private ComboBox<Employe> combo_employe;
	@FXML
	private TextField numEmplacement;

	@FXML
	private Button bnAjoutConf;
	@FXML
	private Button bnAnnuler;

	private Stage stage;

	public static ButtonEmp buttonEmp;
	private Emplacement emplacement;
	private Client client;
	private Employe employe;
	private Reservation reservation;


	@Override
	public void initialize(URL location, ResourceBundle resources) {
		for(Client cli : this.getListClient()) {
			this.combo_client.getItems().add(cli);
		}
		for(Employe emp : this.getListEmploye()) {
			this.combo_employe.getItems().add(emp);
		}
		this.numEmplacement.setText(Integer.toString(buttonEmp.getUnEmpl().getNumEmplacement()));
		this.bnAjoutConf.setDisable(true);
		
		Callback<DatePicker, DateCell> dayCellFactory= this.getDayCellFactory();
        this.dateArr.setDayCellFactory(dayCellFactory);
        this.dateDep.setDayCellFactory(dayCellFactory);
	}


	public AjoutClientEmplController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/AjouterClientEmpl.fxml"));

			loader.setController(this);
			this.stage.initModality(Modality.APPLICATION_MODAL);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("Réservation");

			this.combo_client.setOnAction(e -> {
				if(		!this.numEmplacement.getText().isEmpty() &&
						!this.combo_client.getSelectionModel().getSelectedItem().getNom().isEmpty() &&
						!this.combo_employe.getSelectionModel().getSelectedItem().getNom().isEmpty() &&
						!this.dateArr.getValue().equals(null) &&
						!this.dateDep.getValue().equals(null)
						) {
					
					this.bnAjoutConf.setDisable(false);
				}else {
					this.bnAjoutConf.setDisable(true);
				}

			});

			this.combo_employe.setOnAction(e -> {
				if(		!this.numEmplacement.getText().isEmpty() &&
						!this.combo_client.getSelectionModel().getSelectedItem().getNom().isEmpty() &&
						!this.combo_employe.getSelectionModel().getSelectedItem().getNom().isEmpty() &&
//						!this.dateArr.getValue().equals(null) &&
						!this.dateDep.getValue().equals(null)
						) {
					
					this.bnAjoutConf.setDisable(false);
				}else {
					this.bnAjoutConf.setDisable(true);
				}

			});

			this.dateArr.setOnAction(e -> {
				if(		!this.numEmplacement.getText().isEmpty() &&
						!this.combo_client.getSelectionModel().getSelectedItem().getNom().isEmpty()  &&
						!this.combo_employe.getSelectionModel().getSelectedItem().getNom().isEmpty() &&
						!this.dateArr.getValue().equals(null) &&
						!this.dateDep.getValue().equals(null)
						) {
					this.bnAjoutConf.setDisable(false);
				}else {
					this.bnAjoutConf.setDisable(true);
				}
			});

			this.dateDep.setOnAction(e -> {
				if(		!this.numEmplacement.getText().isEmpty() &&
						!this.combo_client.getSelectionModel().getSelectedItem().getNom().isEmpty()	&&
						!this.combo_employe.getSelectionModel().getSelectedItem().getNom().isEmpty() &&
						!this.dateArr.getValue().equals(null) &&
						!this.dateDep.getValue().equals(null)
						) {
					this.bnAjoutConf.setDisable(false);
				}else {
					this.bnAjoutConf.setDisable(true);
				}
			});


		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	ObservableList<Client> getListClient(){
		ObservableList<Client> listCli = FXCollections.observableArrayList();
		for(Client cli : Data.recupererClients()) {
			listCli.add(cli);
		}
		return listCli;
	}
	@FXML
	ObservableList<Employe> getListEmploye(){
		ObservableList<Employe> listEmp = FXCollections.observableArrayList();
		for(Employe emp : Data.recupererEmployes()) {
			listEmp.add(emp);
		}
		return listEmp;
	}

	@FXML
	void creerReserv(ActionEvent e) {
		buttonEmp.getUnEmpl().setClientAttribue(true);
		Reservation r = new Reservation(buttonEmp.getUnEmpl().getNumEmplacement(), 
										buttonEmp.getUnEmpl().getType(), 
										this.combo_client.getSelectionModel().getSelectedItem(), 
										this.combo_employe.getSelectionModel().getSelectedItem(), 
										this.dateArr.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString(), 
										this.dateDep.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")).toString());
		Data.ajouterReservation(r);
		buttonEmp.setReservation(r);
		buttonEmp.getUnEmpl().setDispo("Réservé");
		stage.close();
	}
	
	private Callback<DatePicker, DateCell> getDayCellFactory() {
		 
        final Callback<DatePicker, DateCell> dayCellFactory = new Callback<DatePicker, DateCell>() {
 
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
 
                        // Disable Monday, Tueday, Wednesday.
                        if (item.getYear() < LocalDate.now().getYear()) {
                            this.setDisable(true);
                            this.setStyle("-fx-background-color: #ffc0cb;");
                        } else if (item.getYear() == LocalDate.now().getYear()) {
                        	if(item.getDayOfYear() < LocalDate.now().getDayOfYear()) {
                        		this.setDisable(true);
                                this.setStyle("-fx-background-color: #ffc0cb;");
                        	}
                        }
                    }
                };
            }
        };
        return dayCellFactory;
    }

	@FXML
	void annuler(ActionEvent e) {
		stage.close();
	}

	// GETTER 

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public Emplacement getEmplacement() {
		return this.emplacement;
	}
	public Client getClient() {
		return this.client;
	}

	public Employe getEmploye() {
		return this.employe;
	}
	
	public Reservation getReservation() {
		return this.reservation;
	}

}
