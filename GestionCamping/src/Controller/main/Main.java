package Controller.main;

import Controller.MenuController;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {
	
	
	
	private static Stage stage;
	@Override
	public void start(Stage stage) {
		try {
			MenuController menu = new MenuController();
			menu.getStage().show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Application.launch(args);
	}
	
	public void showListeClient() {
		stage.close();
	}

}
