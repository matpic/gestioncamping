package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controller.model.Client;
import Controller.model.Data;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ListeClientController extends Stage implements Initializable {
	ObservableList<Client> data = FXCollections.observableArrayList();

	private Stage stage;

	@FXML
	public TableView<Client> tableClient;

	@FXML
	private TableColumn<Client, Integer> numCli;

	@FXML
	private TableColumn<Client, String> nomCli;

	@FXML
	private TableColumn<Client, String> prenomCli;

	@FXML
	private TableColumn<Client, Integer> codePostCli;

	@FXML
	private TableColumn<Client, String> telCli;

	@FXML
	private Button btn_modif;

	@FXML
	private Button btn_suppr;

	public ListeClientController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ListeClient.fxml"));

			loader.setController(this);
			Scene scene = new Scene(loader.load());
			
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Client");
			stage.setMaximized(true);
			this.tableClient.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

		}catch (IOException e) {
			e.printStackTrace();
		}

	}
	//  ------ INITIALISATION TABLEVIEW --------

	public void initialisation_colonnes() {
		numCli.setCellValueFactory(new PropertyValueFactory<>("numClient"));
		nomCli.setCellValueFactory(new PropertyValueFactory<>("nom"));
		prenomCli.setCellValueFactory(new PropertyValueFactory<>("prenom"));
		codePostCli.setCellValueFactory(new PropertyValueFactory<>("codePostal"));
		telCli.setCellValueFactory(new PropertyValueFactory<>("telephone"));
	}

	@FXML
	ObservableList<Client> getListClient(){
		ObservableList<Client> clients = FXCollections.observableArrayList();
		for(Client c : Data.recupererClients()) {
			clients.add(c);
		}
		return clients;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		initialisation_colonnes();
		this.btn_modif.setDisable(true);
		this.btn_suppr.setDisable(true);
		tableClient.setItems(getListClient());
		tableClient.setOnMouseClicked(e -> {
			disableButtons();
		});
	}

	private void disableButtons() {
		if(tableClient.getSelectionModel().getSelectedItem()!=null) {
			btn_suppr.setDisable(false);
		} else {
			btn_suppr.setDisable(true);
		}

		if (tableClient.getSelectionModel().getSelectedItems().size() != 0 || 
				tableClient.getItems().size() != 0 || 
				tableClient.getSelectionModel().getSelectedItem()!=null) {
			if(tableClient.getSelectionModel().getSelectedItems().size() == 1) {
				btn_modif.setDisable(false);
			} else {
				btn_modif.setDisable(true);
			}
		}
	}

	public void refreshTable() {
		data.clear();
		data = Data.obsListClients();
		tableClient.setItems(data);
	}

	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.DELETE){ 
			this.btn_suppr.fire();
	     }
	}

	//	------ CONTROLLER POUR MENU DE NAVIGATION --------



	@FXML
	void retourAuMenu(ActionEvent event) throws IOException {
		MenuController menu = new MenuController();
		menu.getStage().show();
		this.stage.close();

	}


	@FXML
	void listeEmployeScene(ActionEvent event) throws IOException {    	
		ListeEmployeController listeEmp = new ListeEmployeController();
		listeEmp.getStage().show();
		this.stage.close();

	}

	@FXML
	void listeReservationScene(ActionEvent event) throws IOException {
		ListeReservController listeReserv = new ListeReservController();
		listeReserv.getStage().show();
		this.stage.close();

	}

	@FXML
	void gererEmplacementScene(ActionEvent event) throws IOException {
		GererEmplacementController gererEmplacement = new GererEmplacementController();
		gererEmplacement.getStage().show();
		this.stage.close();

	}

	// ------------------------------------------------------------------------
	// ----------- CONTROLLER AJOUTER MODIFIER ET SUPPRIMER CLIENT ------------

	@FXML
	void ouvrirModifClient(ActionEvent event) throws IOException {
		ModifierClientController modifCli = new ModifierClientController(this.tableClient.getSelectionModel().getSelectedItem());
		modifCli.getStage().show();
		modifCli.getStage().setOnHidden(e -> {
			this.refreshTable();
		});
	}


	@FXML
	void ouvrirAjouterClient(ActionEvent event) throws IOException {
		AjoutClientController cliController = new AjoutClientController();
		cliController.getStage().show();
		cliController.getStage().setOnHidden(e -> {
			this.refreshTable();
		});
	}

	@FXML
	void supprimerClient(ActionEvent event) throws IOException {	
		Alert alert = new Alert(AlertType.CONFIRMATION, "Voulez vous supprimer un client ?");
		alert.showAndWait().filter(response -> response == ButtonType.OK)
		.ifPresent(response -> {
			for(Client cli : tableClient.getSelectionModel().getSelectedItems()) {
				int id = cli.getNumClient();
				Data.supprimerClient(id);
			}
			this.refreshTable();
		});
	}



	// ----------------------------------------------------------------



	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}


}
