package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import Controller.model.Client;
import Controller.model.Data;
import Controller.model.Employe;
import Controller.model.Reservation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import javafx.stage.Stage;

public class ListeReservController extends Stage implements Initializable {

	ObservableList<Reservation> data = FXCollections.observableArrayList();

	private Stage stage;
	
	@FXML
	private TableView<Reservation> table_res;
	
	@FXML
	private TableColumn<Reservation, Integer> res_num;
	
	@FXML
	private TableColumn<Reservation, Integer> res_emplacement;
	
	@FXML
	private TableColumn<Reservation, String> res_type;
	
	@FXML
	private TableColumn<Reservation, Client> res_cli;
	
	@FXML
	private TableColumn<Reservation, Employe> res_emp;
	
	@FXML
	private TableColumn<Reservation, String> res_dateA;
	
	@FXML
	private TableColumn<Reservation, String> res_dateD;
	
	@FXML
	private Button btn_suppr;
	
	@FXML
	private Button btn_modif;
	
	public ListeReservController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ListeReserv.fxml"));
			
			loader.setController(this);
			Scene scene = new Scene(loader.load());
			
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Liste des réservations");
			stage.setMaximized(true);
			this.table_res.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
					
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void initialisation_colonnes() {
        res_num.setCellValueFactory(new PropertyValueFactory<>("numReservation"));
        res_emplacement.setCellValueFactory(new PropertyValueFactory<>("num_empl"));
        res_type.setCellValueFactory(new PropertyValueFactory<>("type_empl"));
        res_cli.setCellValueFactory(new PropertyValueFactory<>("client"));
        res_emp.setCellValueFactory(new PropertyValueFactory<>("employe"));
        res_dateA.setCellValueFactory(new PropertyValueFactory<>("dateArrivee"));
        res_dateD.setCellValueFactory(new PropertyValueFactory<>("dateDepart"));
        
    }
	
	@FXML
    ObservableList<Reservation> getListReservations(){
        ObservableList<Reservation> res = FXCollections.observableArrayList();
        for(Reservation r : Data.recupererReservations()) {
            res.add(r);
        }
        
        return res;
    }
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initialisation_colonnes();
		this.btn_modif.setDisable(true);
		this.btn_suppr.setDisable(true);
		this.table_res.setItems(getListReservations());
		this.table_res.setOnMouseClicked(e -> {
			disableButtons();
		});
	}

	private void disableButtons() {
    	if(table_res.getSelectionModel().getSelectedItem()!=null) {
			btn_suppr.setDisable(false);
		} else {
			btn_suppr.setDisable(true);
		}
    	
		if (table_res.getSelectionModel().getSelectedItems().size() != 0 || table_res.getItems().size() != 0 || table_res.getSelectionModel().getSelectedItem()!=null) {
			if(table_res.getSelectionModel().getSelectedItems().size() == 1) {
				btn_modif.setDisable(false);
			} else {
				btn_modif.setDisable(true);
			}
		}
	}
	
	 
	public void refreshTable() {
        data.clear();
        data = Data.obsListReservations();
        table_res.setItems(data);
    }
	
	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.DELETE){ 
			this.btn_suppr.fire();
	     }
	}
	
	@FXML
    void listeClientScene(ActionEvent event) throws IOException {    	
    	ListeClientController listeCli = new ListeClientController();
    	listeCli.getStage().show();
    	this.stage.close();

    }
 

    @FXML
    void listeEmployeScene(ActionEvent event) throws IOException {    	
    	ListeEmployeController listeEmp = new ListeEmployeController();
    	listeEmp.getStage().show();
    	this.stage.close();

    }
    
    @FXML
    void gererEmplacementScene(ActionEvent event) throws IOException {
    	GererEmplacementController gererEmplacement = new GererEmplacementController();
    	gererEmplacement.getStage().show();
    	this.stage.close();

    }
    
    @FXML
    void retourAuMenu(ActionEvent event) throws IOException {
    	MenuController menu = new MenuController();
    	menu.getStage().show();
    	this.stage.close();

    }
    
    
	
	// ----------------------------------------------------------------
    // ----------- CONTROLLER MODIFIER ET SUPPRIMER RESERVATION -------
    
    @FXML
    void modifierReservation(ActionEvent event) throws IOException {
    	ModifierReservationController reservController = new ModifierReservationController(this.table_res.getSelectionModel().getSelectedItem());
    	reservController.getStage().show();
    	reservController.getStage().setOnHidden(e -> {
    		this.refreshTable();
    	});
    }
    
    @FXML
    void supprimerReservation(ActionEvent event) throws IOException {
    	Alert alert = new Alert(AlertType.CONFIRMATION, "Voulez vous supprimer une réservation ?");
		alert.showAndWait().filter(response -> response == ButtonType.OK)
		.ifPresent(response -> {
			ArrayList<Reservation> reserv = new ArrayList<Reservation>();
			for(Reservation res : table_res.getSelectionModel().getSelectedItems()) {
				reserv.add(res);
			}
			Data.supprimerListReservation(reserv);
	        refreshTable();
		});
    }


	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}
			
		
    // ----------------------------------------------------------------
    
}
