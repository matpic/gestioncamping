package Controller.view;


import Controller.model.Emplacement;
import Controller.model.Reservation;
import javafx.scene.control.Button;

public class ButtonEmp extends Button{
	
	private Emplacement unEmpl;
	private Reservation reservation;
	
	public ButtonEmp(Emplacement empl, String titre) {
		super(titre);
		this.reservation = null;
		this.unEmpl = empl;
	}
	
	public ButtonEmp() {
		this.unEmpl = null;
	}
	
	
	public void setBackVert() {
		this.setStyle("-fx-background-color: green; -fx-text-fill:white;");
	}
	
	public void setBackOrange() {
		this.setStyle("-fx-background-color: orange;");
	}
	
	public void setBackRouge() {
		this.setStyle("-fx-background-color: red; -fx-text-fill:white;");
	}
	
	// GETTERS & SETTERS
	
	public Emplacement getEmplacement() {
		return this.unEmpl;
	}
	
	public String getStringNum() {
		return Integer.toString(this.unEmpl.getNumEmplacement());
	}
	
	public String getClient() {
		if (this.unEmpl.getClientAttribue()) {
			return "un client";
		
		}else {
			return "PAS DE CLIENT";
		}
	}
	
	public String getDispo() {
		return this.unEmpl.getDispo();
	}
	
	public String getType() {
		return this.unEmpl.getType();
	}
	
	public void setNum(int i) {
		this.unEmpl.setNumEmplacement(i);
	}

	public Emplacement getUnEmpl() {
		return unEmpl;
	}

	public void setUnEmpl(Emplacement unEmpl) {
		this.unEmpl = unEmpl;
	}
	
	public void setReservation(Reservation reservation) {
		this.reservation = reservation;
	}
	
	public Reservation getReservation() {
		return this.reservation;
	}
	
	

}
