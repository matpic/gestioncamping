package Controller;


import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controller.model.Data;
import Controller.model.Employe;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AjoutEmployeController extends Stage implements Initializable{

	@FXML
	private TextField tNom;
	@FXML
	private TextField tPrenom;
	@FXML
	private Button bnAjouter;
	@FXML
	private Button bnAnnuler;
	@FXML
	private Label label_conf;
	
	private Stage stage;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.bnAjouter.setDisable(true);
		this.label_conf.setVisible(false);
	}
	
	
	public AjoutEmployeController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/AjouterEmploye.fxml"));
			
			loader.setController(this);
			Scene scene = new Scene(loader.load());
			
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Ajouter un employ�");
			this.stage.initModality(Modality.APPLICATION_MODAL);
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		this.tNom.setOnKeyReleased(e -> {
			
			if(		this.tPrenom.getText().isEmpty() ||
					this.tNom.getText().isEmpty()) {
				
				this.bnAjouter.setDisable(true);
			
			}else {
				this.bnAjouter.setDisable(false);
			}
				
			if (contientChiffre(this.tNom.getText())){
				this.bnAjouter.setDisable(true);
				this.label_conf.setVisible(true);
				this.tNom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tNom.styleProperty().set(null);
					
			}
				
			
		});
		
		this.tPrenom.setOnKeyReleased(e -> {
			
			if(		this.tPrenom.getText().isEmpty() ||
					this.tNom.getText().isEmpty()) {
				
				this.bnAjouter.setDisable(true);
			
			}else {
				this.bnAjouter.setDisable(false);
			}
				
			if (contientChiffre(this.tPrenom.getText())){
				this.bnAjouter.setDisable(true);
				this.label_conf.setVisible(true);
				this.tPrenom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tPrenom.styleProperty().set(null);
					
			}			
		});
		
		
	}
	
	public boolean contientChiffre(String str) {
		return (!str.matches("[a-zA-Z-�-]*"));
	}
	
	public boolean contientLettre(String str) {
		return (!str.matches("[0-9]*"));
	}
	
	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){ 
			stage.close();
	     }
		
		if (key.getCode() == KeyCode.ENTER){
			this.bnAjouter.fire();
	     }
		
	}
	
	@FXML
	public void ajouterEmploye(ActionEvent e) {
		
		String nom = tNom.getText();
		String prenom = tPrenom.getText();

		Data.ajouterEmploye(new Employe(nom, prenom));

		Node source = (Node) e.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

	@FXML
	public void activerBtn(Event e) {
		if(this.tPrenom.getText().isEmpty() || this.tNom.getText().isEmpty()) {
			this.bnAjouter.setDisable(true);
		}else {
			this.bnAjouter.setDisable(false);
		}
	}

	@FXML
	public void annuler(ActionEvent e) {
		Node source = (Node) e.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}
	public Stage getStage() {
		return stage;
	}
	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
}
