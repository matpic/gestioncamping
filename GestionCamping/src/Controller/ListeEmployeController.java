package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import Controller.model.Data;
import Controller.model.Employe;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class ListeEmployeController extends Stage implements Initializable{
	
	ObservableList<Employe> data = FXCollections.observableArrayList();
	
	@FXML
	private Button btn_suppr;
	
	@FXML
	private Button btn_modif;
	
	@FXML
	private TableView<Employe> table_emp;
	
	@FXML
	private TableColumn<Employe, Integer> emp_num;
	
	@FXML
	private TableColumn<Employe, String> emp_nom;
	
	@FXML
	private TableColumn<Employe, String> emp_prenom;
	
	private Stage stage;
	
	public ListeEmployeController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ListeEmploye.fxml"));
			
			loader.setController(this);
			Scene scene = new Scene(loader.load());
			
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Liste des employ�s");
			stage.setMaximized(true);
			this.table_emp.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
			
		}catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void initialisation_colonnes() {
        emp_num.setCellValueFactory(new PropertyValueFactory<>("numEmploye"));
        emp_nom.setCellValueFactory(new PropertyValueFactory<>("nom"));
        emp_prenom.setCellValueFactory(new PropertyValueFactory<>("prenom"));
    }
	

	@FXML
    ObservableList<Employe> getListEmployes(){
        ObservableList<Employe> emp = FXCollections.observableArrayList();
        for(Employe e : Data.recupererEmployes()) {
            emp.add(e);
        }
        return emp;
    }
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		initialisation_colonnes();
		this.btn_modif.setDisable(true);
		this.btn_suppr.setDisable(true);
		
		table_emp.setItems(getListEmployes());
		table_emp.setOnMouseClicked(e -> {
			disableButtons();
		});
	}
	
    private void disableButtons() {
    	if(table_emp.getSelectionModel().getSelectedItem()!=null) {
			btn_suppr.setDisable(false);
		} else {
			btn_suppr.setDisable(true);
		}
    	
		if (table_emp.getSelectionModel().getSelectedItems().size() != 0 || table_emp.getItems().size() != 0 || table_emp.getSelectionModel().getSelectedItem()!=null) {
			if(table_emp.getSelectionModel().getSelectedItems().size() == 1) {
				btn_modif.setDisable(false);
			} else {
				btn_modif.setDisable(true);
			}
		}
	}
	
	public void refreshTable() {
        data.clear();
        data = Data.obsListEmployes();
        table_emp.setItems(data);
    }
	
	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.DELETE){ 
			this.btn_suppr.fire();
	     }
	}	
	@FXML
    void listeClientScene(ActionEvent event) throws IOException {    	
    	ListeClientController listeCli = new ListeClientController();
    	listeCli.getStage().show();
    	this.stage.close();

    }
 

	@FXML
    void retourAuMenu(ActionEvent event) throws IOException {
    	MenuController menu = new MenuController();
    	menu.getStage().show();
    	this.stage.close();

    }
    
    @FXML
    void listeReservationScene(ActionEvent event) throws IOException {
    	ListeReservController listeReserv = new ListeReservController();
    	listeReserv.getStage().show();
    	this.stage.close();

    }
    
    @FXML
    void gererEmplacementScene(ActionEvent event) throws IOException {
    	GererEmplacementController gererEmplacement = new GererEmplacementController();
    	gererEmplacement.getStage().show();
    	this.stage.close();

    }
    	
		
		
	// ----------------------------------------------------------------
    // ----------- CONTROLLER AJOUTER MODIFIER ET SUPPRIMER EMPLOYE ---
    
    @FXML
    void ouvrirModifEmploye(ActionEvent event) throws IOException {
    	ModifierEmployeController empController = new ModifierEmployeController(this.table_emp.getSelectionModel().getSelectedItem());
    	empController.getStage().show();
    	empController.getStage().setOnHidden(e -> {
    		this.refreshTable();
    	});
    	
    }
    
    @FXML
    void ouvrirAjouterEmploye(ActionEvent event) throws IOException {
    	AjoutEmployeController empController = new AjoutEmployeController();
    	empController.getStage().show();
    	empController.getStage().setOnHidden(e -> {
    		this.refreshTable();
    	});
    }
    
    @FXML
    void supprimerEmploye(ActionEvent event) throws IOException {
    	Alert alert = new Alert(AlertType.CONFIRMATION, "Voulez vous supprimer un employ� ?");
		alert.showAndWait().filter(response -> response == ButtonType.OK)
		.ifPresent(response -> {
			for(Employe e : table_emp.getSelectionModel().getSelectedItems()) {
				int id = e.getNumEmploye();
				Data.supprimerEmploye(id);
			}
	        
	        refreshTable();
		});
    }


	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}
		


	
    
    // ----------------------------------------------------------------
    
    
    
    
    
    
    
    
    
}