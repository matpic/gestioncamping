package Controller;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainMenuScene extends Application {
	
	private Stage primaryStage;
	private GridPane rootLayout;

	@Override
	public void start(Stage stage) {
		
		stage.setTitle("Test");
		stage.setMaximized(true);
		
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("view/Menu.fxml"));
			rootLayout = (GridPane) loader.load();
			
			Scene scene = new Scene(rootLayout);
			stage.setScene(scene);
			stage.show();
					
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Stage getPrimaryStage() {
		return primaryStage;
	}

	public static void main(String[] args) {
		launch(args);
	}
}
