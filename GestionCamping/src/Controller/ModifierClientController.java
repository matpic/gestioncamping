package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import com.gluonhq.charm.glisten.control.TextField;

import Controller.model.Client;
import Controller.model.Data;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModifierClientController extends Stage implements Initializable{

	static Data data;
	
	private Stage stage;
	
	@FXML
	private TextField txt_nom;
	@FXML
	private TextField txt_prenom;
	@FXML
	private TextField txt_cp;
	@FXML
	private TextField txt_tel;
	@FXML
	private Button bn_modif;
	@FXML
	private Label label_conf;
	
	private Client c;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		this.txt_nom.setText(c.getNom());
		this.txt_prenom.setText(this.c.getPrenom());
		this.txt_cp.setText(Integer.toString(c.getCodePostal()));
		this.txt_tel.setText(c.getTelephone());
		this.label_conf.setVisible(false);
	}
	
	public ModifierClientController(Client client) {
		stage = new Stage();
		this.c = client;
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ModifierClient.fxml"));
			
			loader.setController(this);
			this.stage.initModality(Modality.APPLICATION_MODAL);
			Scene scene = new Scene(loader.load());
			
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Modifier un client");
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		this.txt_nom.setOnKeyReleased(e -> {
			
			if(		this.txt_prenom.getText().isEmpty() ||
					this.txt_nom.getText().isEmpty() ||
					this.txt_cp.getText().isEmpty() ||
					this.txt_tel.getText().isEmpty() 
					){
				
				this.bn_modif.setDisable(true);
			
			}else {
				this.bn_modif.setDisable(false);
			}
				
			if (contientChiffre(this.txt_nom.getText())){
				this.bn_modif.setDisable(true);
				this.label_conf.setVisible(true);
				this.txt_nom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.txt_nom.styleProperty().set(null);
					
			}
				
			
		});
		
		this.txt_prenom.setOnKeyReleased(e -> {
			
			if(		this.txt_prenom.getText().isEmpty() ||
					this.txt_nom.getText().isEmpty() ||
					this.txt_cp.getText().isEmpty() ||
					this.txt_tel.getText().isEmpty() 
					){
				
				this.bn_modif.setDisable(true);
			
			}else {
				this.bn_modif.setDisable(false);
			}
				
			if (contientChiffre(this.txt_prenom.getText())){
				this.bn_modif.setDisable(true);
				this.label_conf.setVisible(true);
				this.txt_prenom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.txt_prenom.styleProperty().set(null);
					
			}
				
			
		});
		
		this.txt_cp.setOnKeyReleased(e -> {
			
			if(		this.txt_prenom.getText().isEmpty() ||
					this.txt_nom.getText().isEmpty() ||
					this.txt_cp.getText().isEmpty() ||
					this.txt_tel.getText().isEmpty() 
					){
				this.bn_modif.setDisable(true);
			
			}else {
				this.bn_modif.setDisable(false);
			}
				
			if (contientLettre(this.txt_cp.getText())){
				this.bn_modif.setDisable(true);
				this.label_conf.setVisible(true);
				this.txt_cp.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.txt_cp.styleProperty().set(null);
					
			}
				
			
		});

		this.txt_tel.setOnKeyReleased(e -> {
			
			if(		this.txt_prenom.getText().isEmpty() ||
					this.txt_nom.getText().isEmpty() ||
					this.txt_cp.getText().isEmpty() ||
					this.txt_tel.getText().isEmpty() 
					){
				
				this.bn_modif.setDisable(true);
			
			}else {
				this.bn_modif.setDisable(false);
			}
				
			if (contientLettre(this.txt_tel.getText())){
				this.bn_modif.setDisable(true);
				this.label_conf.setVisible(true);
				this.txt_tel.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.txt_tel.styleProperty().set(null);
					
			}
				
			
		});
	}
	
	public boolean contientChiffre(String str) {
		return (!str.matches("[a-zA-Z-�-]*"));
	}
	
	public boolean contientLettre(String str) {
		return (!str.matches("[0-9]*"));
	}

	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){ 
			stage.close();
	     }
		
		if (key.getCode() == KeyCode.ENTER){
			this.bn_modif.fire();
	     }
		
	}
	
	@FXML
	public void modifierClient(ActionEvent e) {
		c.setNom(this.txt_nom.getText());
		c.setPrenom(this.txt_prenom.getText());
		c.setCodePostal(Integer.parseInt(this.txt_cp.getText()));
		c.setTelephone(this.txt_tel.getText());
		Data.modifierClient(c);
		stage.close();
	}
	
	@FXML
	public void annuler(ActionEvent e)  {
		stage.close();
	}
	
	
	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
}
