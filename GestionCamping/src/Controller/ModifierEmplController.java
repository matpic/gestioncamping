package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controller.model.Data;
import Controller.model.Emplacement;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ModifierEmplController extends Stage implements Initializable{

	private Stage stage;

	@FXML
	TextField modif_num;
	@FXML
	ComboBox<String> combo_type;
	@FXML
	private Button bn_modif;

	private Emplacement e;


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {

		this.modif_num.setText(Integer.toString(e.getNumEmplacement()));
		this.combo_type.getItems().addAll("Tente", "Caravane", "Mobil-Home");
		this.combo_type.setPromptText(e.getType());

	}

	public ModifierEmplController(Emplacement emp) {
		this.e = emp;

		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ModifierEmpl.fxml"));

			loader.setController(this);
			this.stage.initModality(Modality.APPLICATION_MODAL);
			Scene scene = new Scene(loader.load());

			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});

			stage.setScene(scene);
			stage.setTitle("Modifier Emplacement");

		}catch (IOException e) {
			e.printStackTrace();
		}


	}

	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){ 
			stage.close();
		}

		if (key.getCode() == KeyCode.ENTER){
			this.bn_modif.fire();
		}

	}

	@FXML
	public void confModif(ActionEvent event) {
		this.e.setType(this.combo_type.getSelectionModel().getSelectedItem());
		Data.modifierEmplacement(this.e);
		stage.close();
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	@FXML
	void annuler(ActionEvent e) {
		stage.close();
	}




}
