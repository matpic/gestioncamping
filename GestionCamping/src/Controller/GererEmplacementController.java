package Controller;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;
import Controller.model.Data;
import Controller.model.Emplacement;
import Controller.model.Reservation;
import Controller.view.ButtonEmp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class GererEmplacementController extends Stage implements Initializable{

	private final int TAILLE = 20;
	
	

	@FXML
	private GridPane grid;
	@FXML
	private TextField txt_empl;
	@FXML
	private TextField empl_client;
	@FXML
	private TextField empl_type;
	@FXML
	private TextField empl_disp;
	@FXML
	private Button btn_modif;
	@FXML
	private Button btn_ajout;
	@FXML
	private Button btn_annuler;
	@FXML
	private Button bn_suppr;
	
	private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	private Stage stage;

	private ButtonEmp grille[][] = new ButtonEmp[TAILLE][TAILLE];
	
	public GererEmplacementController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ListeEmplacement.fxml"));
			loader.setController(this);
			stage.setScene(new Scene(loader.load()));
			stage.setTitle("Camping");
			stage.setMaximized(true);
		}catch (IOException e) {
			e.printStackTrace();
		}
		
		this.grid.setOnMouseClicked(e -> {	
			this.btn_ajout.setDisable(false);
		});
	}


	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.initialisationCamping();
		this.btn_modif.setDisable(true);
		this.btn_ajout.setDisable(true);
		this.bn_suppr.setDisable(true);
	}
	
	
	private void initialisationCamping() {
		int j = 0;
		int indice_emp = 1;
		ArrayList<Emplacement> listEmp = Data.recupererEmplacements();
		ArrayList<Reservation> listRes = Data.recupererReservations();
		for (int i = 0; i < TAILLE; i++) {
			for (j = 0; j < TAILLE; j++) {
				
				ButtonEmp b = new ButtonEmp();
				b.setPrefWidth(54);
				b.setPrefHeight(48);
				b.setDisable(true);
				grille[i][j] = b;
		
				//PISCINE
				if (((i >= 1) && (i <= 5)) && ((j >= 1) && (j <= 8)) ) {
					b.setText(null);
					b.setStyle("-fx-background-color: deepskyblue;");
				}
				
				//ENTREE
				if (((i == 19)) && ((j >= 16) && (j <= 19)) ) {
					b.setText(null);
					b.setStyle("-fx-background-color: black;");
				}
				
				//ACCUEIL
				
				//EMPLACEMENT
				if (	((((i >= 7) && (i <= 8)) || ((i >= 10) && (i <= 11)) || ((i >= 13) && (i <= 14))) && (((j >= 1) && (j <= 8)) ||
						((j >= 11) && (j <= 18))))
						//Zone en haut au droite
						|| ((((i >= 1) && (i <= 2)) || ((i >= 4) && (i <= 5))) && ((j >= 11) && (j <= 18)))
						) {
					b.setDisable(false);
					b.setUnEmpl(listEmp.get(indice_emp-1));
					b.getStyleClass().add("button_emp_lock");
					
					for(Reservation r : listRes) {
						if(r.getNum_empl() == b.getUnEmpl().getNumEmplacement()) {
							b.setReservation(r);
							String dateArr = b.getReservation().getDateArrivee();
							LocalDate date = LocalDate.now();
							String dateDuJour = dateFormat.format(date);
							if(dateArr.compareTo(dateDuJour) > 0) {
								b.getUnEmpl().setDispo("R�serv�");
							} else {
								b.getUnEmpl().setDispo("Occup�");
							}
						}
					}
					
					
					b.setText(Integer.toString(b.getUnEmpl().getNumEmplacement()));
					initCouleurBouton(b);
					indice_emp++;	
				}
				
				b.setOnAction(e ->{
					AjoutClientEmplController.buttonEmp = b;
					try {
						this.afficherInfoEmplacement(e);
						if(b.getReservation()!=null) {
							this.bn_suppr.setDisable(false);
						}else {
							this.bn_suppr.setDisable(true);
						}
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				});
				grid.add(b, j, i);
			}
		}
		
	}


	public void afficherInfoEmplacement(ActionEvent event) throws IOException  {
		int j = 0;
		int i = 0;
		boolean trouve = false;
		while(j < TAILLE && !(trouve)) {
			i = 0;
			while(i < TAILLE && !(trouve)) {
				ButtonEmp b = new ButtonEmp();
				b = grille[i][j];
				
				if(event.getSource().equals(b)) {					
					
					this.txt_empl.setText(Integer.toString(b.getUnEmpl().getNumEmplacement()));
					
					if(b.getReservation() != null) {
						this.empl_client.setText(b.getReservation().getClient().toString());
					} else {
						this.empl_client.setText("Pas de Client");
					}
					
					this.empl_disp.setText(b.getUnEmpl().getDispo());
					this.empl_type.setText(b.getUnEmpl().getType());
					trouve = true;
					this.btn_modif.setDisable(false);
					
					if (b.getUnEmpl().getClientAttribue()) {
						this.bn_suppr.setDisable(false);
					}else {
						this.btn_ajout.setDisable(false);
					}
					
					
				}
				i++;
			}
			j++;
		}
	}
	
	public void initCouleurBouton(ButtonEmp b) {
			if(b.getUnEmpl().getDispo().compareTo("R�serv�")==0) {
				b.setBackOrange();
			} else if(b.getUnEmpl().getDispo().compareTo("Occup�")==0){
				b.setBackRouge();
			} else if(b.getUnEmpl().getDispo().compareTo("Disponible")==0) {
				b.setBackVert();
			}
	}
	
	
	private void refreshCamping() {
		this.initialisationCamping();
	}
	
	
	//------- OUVERTURE DES FENETRES ---------
	
	@FXML
	void ouvrirModifEmpl(ActionEvent event) throws IOException {
		Emplacement emplacement = new Emplacement(Integer.parseInt(this.txt_empl.getText()), this.empl_disp.getText(), this.empl_type.getText());
		ModifierEmplController modifEmpl = new ModifierEmplController(emplacement);
		modifEmpl.getStage().show();
		modifEmpl.getStage().setOnHidden(e -> {
			this.refreshCamping();
		});
	}
	
	@FXML
	void ouvrirAjoutClientEmpl(ActionEvent event) throws IOException {
		AjoutClientEmplController ajoutCliEmpl = new AjoutClientEmplController();
		ajoutCliEmpl.getStage().show();
		ajoutCliEmpl.getStage().setOnHidden(e -> {
			this.refreshCamping();
		});
	}
	
	@FXML
	void supprimerReserv(ActionEvent event) throws IOException {
		Alert alert = new Alert(AlertType.CONFIRMATION, "Voulez vous supprimer cette r�servation ?");
		alert.showAndWait().filter(response -> response == ButtonType.OK)
		.ifPresent(response -> {
			if(this.txt_empl.getText() != null){
				Data.supprimerReservationParEmp(Integer.parseInt(this.txt_empl.getText()));
			}
		});
		this.refreshCamping();
	}


	
	//-------------------
	// ----- MENU -------

	@FXML
	void listeClientScene(ActionEvent event) throws IOException {    	
		ListeClientController listeCli = new ListeClientController();
		listeCli.getStage().show();
		this.stage.close();

	}


	@FXML
	void listeEmployeScene(ActionEvent event) throws IOException {    	
		ListeEmployeController listeEmp = new ListeEmployeController();
		listeEmp.getStage().show();
		this.stage.close();

	}

	@FXML
	void listeReservationScene(ActionEvent event) throws IOException {
		ListeReservController listeReserv = new ListeReservController();
		listeReserv.getStage().show();
		this.stage.close();

	}

	@FXML
	void retourAuMenu(ActionEvent event) throws IOException {
		MenuController menu = new MenuController();
		menu.getStage().show();
		this.stage.close();

	}
	
	//----------------------------

	
	public void setButtonEmpDansGrille() {
		
	}

	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}


}
