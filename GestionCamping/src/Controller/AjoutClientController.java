package Controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import Controller.model.Client;
import Controller.model.Data;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class AjoutClientController extends Stage implements Initializable{


	@FXML
	private Button bnAjoutConf;
	@FXML
	private TextField tnom;
	@FXML
	private TextField tprenom;
	@FXML
	private TextField tcp;
	@FXML
	private TextField ttel;
	@FXML
	private Label label_conf;
	
	private Stage stage;

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.bnAjoutConf.setDisable(true);
		this.label_conf.setVisible(false);
	}
	
	
	public AjoutClientController() {
		stage = new Stage();
		try {
			FXMLLoader loader = new FXMLLoader(getClass().getResource("view/AjouterClient.fxml"));

			loader.setController(this);
			this.stage.initModality(Modality.APPLICATION_MODAL);
			
			Scene scene = new Scene(loader.load());
			scene.setOnKeyPressed(e -> {
				this.raccourcisClavier(e);
			});
			
			stage.setScene(scene);
			stage.setTitle("Ajouter un client");

		}catch (IOException e) {
			e.printStackTrace();
		}

		this.tnom.setOnKeyReleased(e -> {
			
			if(		this.tprenom.getText().isEmpty() ||
					this.tnom.getText().isEmpty() ||
					this.tcp.getText().isEmpty() ||
					this.ttel.getText().isEmpty()) {
				
				this.bnAjoutConf.setDisable(true);
			
			}else {
				this.bnAjoutConf.setDisable(false);
			}
				
			if (contientChiffre(this.tnom.getText())){
				this.bnAjoutConf.setDisable(true);
				this.label_conf.setVisible(true);
				this.tnom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tnom.styleProperty().set(null);
					
			}
				
			
		});

		this.tprenom.setOnKeyReleased(e -> {
			
			if(		this.tprenom.getText().isEmpty() ||
					this.tnom.getText().isEmpty() ||
					this.tcp.getText().isEmpty() ||
					this.ttel.getText().isEmpty()) {
				
				this.bnAjoutConf.setDisable(true);
			
			}else {
				this.bnAjoutConf.setDisable(false);
			}
				
			if (contientChiffre(this.tprenom.getText())){
				this.bnAjoutConf.setDisable(true);
				this.label_conf.setVisible(true);
				this.tprenom.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tprenom.styleProperty().set(null);
					
			}
				
			
		});

		this.tcp.setOnKeyReleased(e -> {
			
			if(		this.tprenom.getText().isEmpty() ||
					this.tnom.getText().isEmpty() ||
					this.tcp.getText().isEmpty() ||
					this.ttel.getText().isEmpty()) {
				
				this.bnAjoutConf.setDisable(true);
			
			}else {
				this.bnAjoutConf.setDisable(false);
			}
				
			if (contientLettre(this.tcp.getText())){
				this.bnAjoutConf.setDisable(true);
				this.label_conf.setVisible(true);
				this.tcp.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.tcp.styleProperty().set(null);
					
			}
				
			
		});
		
		this.ttel.setOnKeyReleased(e -> {
			
			if(		this.tprenom.getText().isEmpty() ||
					this.tnom.getText().isEmpty() ||
					this.tcp.getText().isEmpty() ||
					this.ttel.getText().isEmpty()) {
				
				this.bnAjoutConf.setDisable(true);
			
			}else {
				this.bnAjoutConf.setDisable(false);
			}
				
			if (contientLettre(this.ttel.getText())){
				this.bnAjoutConf.setDisable(true);
				this.label_conf.setVisible(true);
				this.ttel.setStyle("-fx-border-color: red;");
			}
			else 
			{
				this.label_conf.setVisible(false);
				this.ttel.styleProperty().set(null);
					
			}
				
			
		});

	}
	
	
	public boolean contientChiffre(String str) {
		return (!str.matches("[a-zA-Z-�-]*"));
	}
	
	public boolean contientLettre(String str) {
		return (!str.matches("[0-9-]*"));
	}
	
	public void raccourcisClavier(KeyEvent key){
		if (key.getCode() == KeyCode.ESCAPE){ 
			stage.close();
	     }
		
		if (key.getCode() == KeyCode.ENTER){
			this.bnAjoutConf.fire();
	     }
		
	}

	@FXML
	void ajouterClient(ActionEvent e) {
		if (!(	tnom.getText().isEmpty()) &&
				!(	tprenom.getText().isEmpty()) &&
				!(	tcp.getText().isEmpty()) &&
				!(	ttel.getText().isEmpty())) {

			String nom = tnom.getText();
			String prenom = tprenom.getText();
			int codepostal = Integer.parseInt(tcp.getText());
			String telephone = ttel.getText();

			Data.ajouterClient(new Client(nom,prenom,codepostal,telephone));

		}else{
			System.out.println("Un des champs est vide");
		}
		Node source = (Node) e.getSource();
		Stage stage = (Stage) source.getScene().getWindow();
		stage.close();
	}

	@FXML
	void annuler(ActionEvent e) {
		stage.close();
	}
	
	// GETTER 

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}



}
